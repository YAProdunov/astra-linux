#!/bin/bash

## Задаем Переменные, определяем домен
domain=`grep search /etc/resolv.conf | awk '{print $2}'`
DOMAIN=${domain^^}

## останавливаем службу sssd
/bin/systemctl stop sssd

## делаем резервную копию текущего конфига
cp /etc/sssd/sssd.conf /etc/sssd/sssd.orig_adsetup

## создаем новое содержимое sssd.conf используя переменные
cat << EOF > /etc/sssd/sssd.conf
[sssd]
domains = $domain
config_file_version = 2
services = nss, pam
default_domain_suffix = $domain

[domain/${domain}]
ad_domain = ${domain}
krb5_realm = ${DOMAIN}
realmd_tags = manages-system joined-with-adcli 
cache_credentials = True
id_provider = ad
krb5_store_password_if_offline = True
default_shell = /bin/bash
ldap_id_mapping = True
use_fully_qualified_names = True
fallback_homedir = /home/%d/%u
access_provider = simple
ignore_group_members = True
krb5_auth_timeout = 20
case_sensitive = false
simple_allow_groups = domain users@${domain}
dyndns_update = true
dyndns_refresh_interval = 43200
dyndns_update_ptr = true
dyndns_ttl = 3600

[pam]
pam_id_timeout = 20

EOF

# Выствляем правильные права на конфигурационный файл sssd.conf
chown root:root /etc/sssd/sssd.conf
chmod 0600 /etc/sssd/sssd.conf

# Запускаем службу
/bin/systemctl start sssd

# Делаем резервную копию конфига krb5.conf
cp /etc/krb5.conf /etc/krb5.orig_adsetup

## создаем новое содержимое krb5.conf используя переменные
cat << EOF > /etc/krb5.conf
#astra
[libdefaults]
    default_realm = ${DOMAIN}
    default_ccache_name = FILE:/tmp/krb5cc_%{uid}
    kdc_timesync = 1
    ccache_type = 4
    forwardable = true
    proxiable = true
    fcc-mit-ticketflags = true
    dns_lookup_realm = false
    dns_lookup_kdc = true
    v4_instance_resolve = false
    v4_name_convert = {
        host = {
            rcmd = host
            ftp = ftp
        }
        plain = {
            something = something-else
        }
    }
    rdns = false

[realms]
    ${DOMAIN} = {
    default_domain = ${DOMAIN}
    }

[domain_realm]
    .${domain} = ${DOMAIN}
    ${domain} = ${DOMAIN}
[login]
    krb4_convert = true
    krb4_get_tickets = false

EOF
